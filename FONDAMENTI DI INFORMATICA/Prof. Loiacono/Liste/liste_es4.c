#include "liste_int.h"
#include <time.h>
#include <string.h>

int get(lista l, int pos);
lista rimuovi_pos(lista l, int pos);

int main()
{
  int i,j,pos;
  lista l=NULL;
  for (i=199; i>=1; i=i-2)
    l = inserisci_in_testa(l,i);
  for (i=2;i<=lunghezza(l,1);i++)
  {
    pos = get(l,i);
    for (j=lunghezza(l,1);j>=1;j--)
      if (j%pos==0)
        l = rimuovi_pos(l,j);
  }
  printf("Numeri fortunati: ");
  stampa(l,1);

  return 0;
}

int get(lista l, int pos)
{
  int i;
  for (i=1; i<pos && l!=NULL; i++)
    l = l->next;
  if (l!=NULL)
    return l->el;
  // se usata non correttamente (pos>lunghezza(l)) restituisce un valore errato
  return -1;
}

lista rimuovi_pos(lista l, int pos)
{
  lista cur=l, pre=NULL;
  int i;
  for (i=1; i<pos && cur!=NULL; i++)
  {
    pre = cur;
    cur = cur->next;
  }
  if (cur!=NULL)
  {
    if (pre==NULL)
      return rimuovi_in_testa(l);
    else
    {
      pre->next = cur->next;
      free(cur);
      return l;
    }
  }
  return l;
}
